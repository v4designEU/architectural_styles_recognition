# This code uses k-fold cross validation to train VGG16 to a given dataset

# necessary imports
import pickle
import h5py
import numpy as np
import os
import keras
import matplotlib.pyplot as plt
import argparse
from imutils import paths
import cv2
from aspectawarepreprocessor import AspectAwarePreprocessor
from imagetoarraypreprocessor import ImageToArrayPreprocessor
from datasets.simpledatasetloader import SimpleDatasetLoader
from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics import classification_report, confusion_matrix, recall_score, precision_score, f1_score, \
    plot_confusion_matrix, ConfusionMatrixDisplay
from keras.models import load_model
from keras import backend as K

# functions used in order to track all these metrics while training phase
def recall_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall

def precision_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + K.epsilon()))

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=False, default= 'F:/CERTH/testing',            #Enter path to dataset(must be a file for random image and a file of files for annotated ones)
                help="path to input dataset")   # in this path, annotated images should be inside folders that have the labels' names(same order as target_names in line 48), if they are not annotated and we want a general prediction, they just have to be into a file
ap.add_argument("-im", "--load_model", required=False, default='C:/Users/kostas/PycharmProjects/training/models/arch/VGG19_arch_sgd{0.001, 256, 130}_styles_transfered__split_1.hdf5',        #Enter path to model.
                help="path to input model")
args = vars(ap.parse_args())

target_names = ["Art_Deco", "Art_Nouveau", "Baroque", "Bauhaus", "Biedermeier", "Corinthian_Order",
                "Deconstructuvism", "Doric_Order", "Early_Roman", "Gothic", "Hellinistic", "Ionic_Order", "Modernist",
                "Neoclassical", "None", "Postmodernism", "Rennaisance",  "Rococo", "Romanesque"]

# grab the list of images that we'll be describing
print("[INFO] loading images...")
images = []
imagePaths = list(paths.list_images(args["dataset"]))
for image in imagePaths:
    name = image.split("\\")[-1]
    images.append(name)
print(images)


# initialize the image preprocessor
sp = AspectAwarePreprocessor(224, 224)
iap = ImageToArrayPreprocessor()

# load data to test
sdl = SimpleDatasetLoader(preprocessors=[sp, iap])
(X, y) = sdl.load(imagePaths, verbose=500)
X = X.astype("float")/255.0
print("[INFO] size of data: ", X.shape)

# convert y from "emotions" to labels from 0-20
y2 = []
for i in range(0, len(y)):
    y2.append(target_names.index(y[i]))

# convert the labels from integers to binary vectors
y = LabelBinarizer().fit_transform(y2)


cvscores = []
# load trained model
print("[INFO] Loading trained model...")
#with open(args['load_model'], 'rb') as pickle_file:
#    model = pickle.load(pickle_file)   # sav files

model = load_model(args['load_model'],
                   custom_objects={ 'f1_m' : f1_m, 'precision_m' : precision_m, 'recall_m' : recall_m})
print("[INFO] Model is loaded!")

# print model summary
#print(model.summary())

# # Evaluate model
# print("[INFO] evaluating network...")
# scores = model.evaluate(X, y, verbose=1)
# print("%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))
# cvscores.append(scores[1] * 100)

#X = X.reshape(272, -1)

# predict values
print("[INFO] predicting values...")
predictions = model.predict(X)
#print(predictions)

data = []
for prediction in predictions:
    data.append(sorted(zip(prediction, target_names), reverse=True)[:1])    #3
print(data)
hc_tp_sum = 0
tp_sum = 0
y_predicted = []

probability = []
for sole_prediction in predictions:
    probability.append(sorted(sole_prediction, reverse=True)[:1])

L = []
for i in range(0, len(predictions)):
    # get predicted label
    #print(predictions[i].argmax(axis=0))
    y_predicted.append(predictions[i].argmax(axis=0))
    print("Predicted Probability and Labels: ", data[i])
    #print("Image names : ", images[i])
    # get true label
    print("True Label: ", target_names[y2[i]])

    if y2[i] == y_predicted[i]:
        tp_sum += 1

print("True Positives: ", tp_sum)
print("Total: ", len(y))

print("[INFO] Recall: {:.2f}%".format(100*recall_score(y2, y_predicted, average='micro')))
print("[INFO] Precision: {:.2f}%".format(100*precision_score(y2, y_predicted, average='micro')))
print("[INFO] F1-measure: {:.2f}%".format(100*f1_score(y2, y_predicted, average='micro')))

confusion_matrix = confusion_matrix(y.argmax(axis=1),
           predictions.argmax(axis=1))
# get confusion matrix
print("====== CONFUSION MATRIX ======")
print(confusion_matrix)

y_conf = y.argmax(axis=1)
predictions_conf = predictions.argmax(axis=1)

# Plot non-normalized confusion matrix
cm_display = ConfusionMatrixDisplay(confusion_matrix, display_labels=target_names).plot()
tick_marks = np.arange(len(target_names))
plt.xticks(tick_marks, rotation=45)
plt.show(cm_display.figure_)
