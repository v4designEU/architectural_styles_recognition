# Architectural Styles Recognition

Source for the V4Design project of the architectural style recognition.

In order to run the architectural recognition script for buildings, we used
Python 3.7, PyCharm IDE(version 193.6911.25) and the environment settings in Env_(1-5).png.

Also inside Architectural_style_predictor.py you'll need to provide the paths to the dataset/images, the model and the csvs where the output information will be displayed.

The model is provided in : 
https://drive.google.com/file/d/1afgpz7k0wx3moz4cX1X3HSRVBEI3YGlu/view?usp=sharing
